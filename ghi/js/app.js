function createCard(name, description, pictureUrl, start, end, location) {
    return `
    <div class="col-4">
      <div class="card" id="${name}">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-body-secondary">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            ${start} - ${end}
        </div>
      </div>
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        let alertPlaceholder = document.getElementById('liveAlertPlaceholder')
        let alertTrigger = true;

        function alert(message, type) {
        let wrapper = document.createElement('div')
        wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

        alertPlaceholder.append(wrapper)
        }



        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const location = details.conference.location.name;
            const startDate = details.conference.starts;
            const s = startDate.slice(0, 10);
            const startList = s.split('-');
            const start = startList[1] + '/' + startList[2] + '/' + startList[0];

            const endDate = details.conference.ends;
            const d = endDate.slice(0, 10);
            const endList = d.split('-');
            const end = endList[1] + '/' + endList[2] + '/' + endList[0];

            const html = createCard(title, description, pictureUrl, start, end, location);

            const column = document.querySelector('.row');
            column.innerHTML += html;
            document.getElementById(title).style.boxShadow = "5px 5px 5px black";
            document.getElementsByClassName("col-4").style.marginBottom = "18px";


          }
        }

      }


    } catch (e) {
      // Figure out what to do if an error is raised
      if (alertTrigger === true) {
        alert('RED ALERT!')
    }


    }

  });
