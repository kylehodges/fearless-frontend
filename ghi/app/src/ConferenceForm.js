import React, { useEffect, useState } from 'react';

function ConferenceForm() {
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }


    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const [starts, setStarts] = useState('');
    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    }

    const [ends, setEnds] = useState('');
    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    }

    const [description, setDescription] = useState('');
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const [maxPresentations, setMaxPresentations] = useState('');
    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }

    const [maxAttendees, setMaxAttendees] = useState('');
    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;
        console.log(data);

        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocation('');

        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);


        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations);
        }
      }



    useEffect(() => {
      fetchData();
    }, []);
    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" id="name" name="name" className="form-control" value={name}/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartsChange} placeholder="Date" required type="date" id="starts" name="starts" className="form-control" value={starts}/>
                <label htmlFor="starts">Start Date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndsChange} placeholder="Date" required type="date" id="ends" name="ends" className="form-control" value={ends}/>
                <label htmlFor="ends">End</label>
              </div>
              <div className="mb-3">
                <input onChange={handleDescriptionChange} required id="description" name="description" className="form-control" type="textarea" value={description}/>
                <label htmlFor="description">Description</label>
              </div>
              <div className="mb-3">
                <input onChange={handleMaxPresentationsChange} required id="max_presentations" name="max_presentations" type="number" className="form-control" value={maxPresentations}/>
                <label htmlFor="max-presentations">Max Presentations</label>
              </div>
              <div className="mb-3">
                <input onChange={handleMaxAttendeesChange} required id="max_attendees" name="max_attendees" type="number" className="form-control" value={maxAttendees}/>
                <label htmlFor="max-attendees">Max Attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} required id="location" name="location" className="form-select" value={location}>
                <option value="">Choose Location</option>
                {locations.map(location => {
                    return (
                        <option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                    )
                })}
                </select>
              </div>

              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>

    );
}

export default ConferenceForm;
